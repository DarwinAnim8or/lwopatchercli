﻿using System;
using System.Net;

namespace lwoPatcherCLI
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("==============================");
			Console.WriteLine("lwoPatcher - CLI edition. v1.0");
			Console.WriteLine("==============================");
			Console.WriteLine("Will automatically download all available alpha client files.");
			Console.WriteLine("");

			WebClient webClient = new WebClient();
			DownloadUtils.DownloadVersions(webClient); //These are always re-downloaded.
			DownloadUtils.DownloadFilesFromTxt(webClient, @"..\versions\version.txt", false);
			DownloadUtils.DownloadFilesFromTxt(webClient, @"..\versions\index.txt", false);

			DownloadUtils.DownloadFilesFromTxt(webClient, @"..\versions\frontend.txt", true);

			DownloadUtils.DownloadFilesFromTxt(webClient, @"..\versions\hotfix.txt", false);

			DownloadUtils.DownloadFilesFromTxt(webClient, @"..\versions\trunk.txt", true);

			webClient.Dispose();
		}
	}
}
